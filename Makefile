DEBUG = 0
UNICODE = 1

CC = gcc
CFLAGS = -pipe -mthreads -Os
LDFLAGS = -mwindows
LIBS = -lkernel32 -luser32 -lgdi32

ifeq ($(DEBUG),1)
MACROS += -D_DEBUG
CFLAGS += -g
else
LDFLAGS += -s -Wl,-Os
endif

ifeq ($(UNICODE), 1)
MACROS += -DUNICODE
endif

all: $(patsubst %.c, %.exe, $(wildcard *.c))

%.exe: %.o
	gcc $(LDFLAGS) $^ $(LIBS) -o $@

%.o: %.c
	gcc $(MACROS) -Wno-attributes $(CFLAGS) -c $^ -o $@

clean:
	@del /q *.exe

.phony: all clean
